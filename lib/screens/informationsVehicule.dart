import 'package:flutter/material.dart';
import 'package:peagetunisie/screens/thePage.dart';
import 'package:peagetunisie/services/baseDeDonnees.dart';

class InfosVehicule extends StatefulWidget {
  String userID = "";
  InfosVehicule({required this.userID});
  @override
  State<StatefulWidget> createState() =>
      InfosVehiculeState(userID: this.userID);
}

class InfosVehiculeState extends State<InfosVehicule> {
  String userID = "";
  InfosVehiculeState({required this.userID});
  @override
  String marque = "";
  String modele = "";
  String couleur = "";
  String matricule = "";

  Widget _buildMarque() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        labelText: 'Marque*',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        marque = val;
      },
      validator: (String? value) {
        return (value != null) ? 'Il y a une erreur' : null;
      },
    );
  }

  Widget _buildModele() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        labelText: 'Modele *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        modele = val;
      },
      validator: (String? value) {
        return (value != null) ? 'Il y a une erreur' : null;
      },
    );
  }

  Widget _buildCouleur() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        labelText: 'Couleur *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        couleur = val;
      },
      validator: (String? value) {
        return (value != null) ? 'Il y a une erreur' : null;
      },
    );
  }

  Widget _buildMatricule() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        labelText: 'Matricule *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        matricule = val;
      },
      validator: (String? value) {
        return (value != null) ? 'Il y a une erreur' : null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Padding(
            padding: EdgeInsets.all(20),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(userID),
              _buildMarque(),
              _buildModele(),
              _buildCouleur(),
              _buildMatricule(),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                  color: Colors.red[200],
                  onPressed: () {
                    modifierInfoUtilisateur();
                  },
                  child: Text("Ajouter infos sur la véhicule",
                      style: TextStyle(color: Colors.white))),
            ])),
      ),
    );
  }

  void modifierInfoUtilisateur() async {
    DatabaseServices ds = DatabaseServices(uid: userID);

    ds.AddVehiculeInfo(marque, modele, couleur, matricule);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ThePage(uid: userID, payOrNot: 1)),
    );
  }
}
