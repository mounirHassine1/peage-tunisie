import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:peagetunisie/screens/Sinscrire.dart';
import 'package:peagetunisie/services/authentificationService.dart';
import 'package:peagetunisie/screens/thePage.dart';

class SeConnecter extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SeConnecterState();
}

class SeConnecterState extends State<SeConnecter> {
  AuthentificationService _auth = AuthentificationService();
  String email = "";
  String motDePasse = "";

  Widget _buildEmail() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.mail),
        hintText: 'Quel est ton email?',
        labelText: 'Email *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        email = val;
      },
      validator: (String? value) {
        return (value != null && value.contains('@') != false)
            ? 'Il y a une erreur'
            : null;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.visibility),
        hintText: 'Quel est ton mot de passe?',
        labelText: 'Mot de passe *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        motDePasse = val;
      },
      validator: (String? value) {
        return (value != null) ? 'Il y a une erreur' : null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Padding(
            padding: EdgeInsets.all(20),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              _buildEmail(),
              _buildPassword(),
              SizedBox(
                height: 50,
              ),
              Row(children: [
                SizedBox(width: 50),
                RaisedButton(
                    color: Colors.red[200],
                    onPressed: () {
                      signInUser();
                    },
                    child: Text("Se connecter",
                        style: TextStyle(color: Colors.white))),
                SizedBox(width: 30),
                TextButton(
                  child:
                      Text("S'inscrire", style: TextStyle(color: Colors.blue)),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Sinscrire()),
                    );
                  },
                )
              ])
            ])),
      ),
    );
  }

  void signInUser() async {
    dynamic result = await _auth.signIn(email, motDePasse);
    if (result != null) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ThePage(
                  uid: result,
                  payOrNot: 1,
                )),
      );
    }
  }
}
