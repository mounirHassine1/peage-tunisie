import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:peagetunisie/services/baseDeDonnees.dart';

class Historique extends StatefulWidget {
  String uid;
  Historique({required this.uid});
  @override
  State<StatefulWidget> createState() => HistoriqueState(uid: this.uid);
}

class HistoriqueState extends State<Historique> {
  final CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('users');
  String uid;
  HistoriqueState({required this.uid});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Historique"),
        ),
        body: SingleChildScrollView(child: searchHistorique()));
  }

  searchHistorique() async {
    StreamBuilder(
      stream: collectionReference.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          return ListView(
            children: snapshot.data!.docs
                .map((e) => Column(
                      children: [
                        ListTile(
                          title: Text(e['name']),
                        ),
                        Divider(
                          color: Colors.black.withOpacity(0.6),
                          thickness: 2,
                        )
                      ],
                    ))
                .toList(),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }
}
