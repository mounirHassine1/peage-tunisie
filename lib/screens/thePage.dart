import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:peagetunisie/main.dart';
import 'package:peagetunisie/screens/profile.dart';
import 'package:peagetunisie/services/authentificationService.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:peagetunisie/services/baseDeDonnees.dart';

class ThePage extends StatefulWidget {
  String uid;
  int payOrNot;
  ThePage({required this.uid, required this.payOrNot});
  @override
  State<StatefulWidget> createState() =>
      ThePageState(uid: this.uid, payOrNot: this.payOrNot);
}

class ThePageState extends State<ThePage> {
  String uid;
  int payOrNot;
  ThePageState({required this.uid, required this.payOrNot});
  AuthentificationService _auth = AuthentificationService();

  Set<Marker> _markers = {};
  LatLng myPosition = LatLng(30, 12);
  //Position currentPosition = Position();
  List<LatLng> listeDePositions = [
    LatLng(35.541623, 11.017245),
    LatLng(35.600962, 11.005035),
    LatLng(35.516964, 11.032476)
  ];
  int montant = 0;

  void locatePosition(GoogleMapController controller) async {
    // Position position = await Geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.high);
    // currentPosition = position;
    //LatLng LatLangPosition = LatLng(position.latitude, position.longitude);
    LatLng LatLangPosition = LatLng(35.600962, 11.005035);
    myPosition = LatLangPosition;
    CameraPosition cameraPosition =
        new CameraPosition(target: LatLangPosition, zoom: 15);
    controller.animateCamera(CameraUpdate.newCameraPosition(cameraPosition));
    if (listeDePositions.contains(LatLangPosition)) {
      String pos = "";

      for (Marker x in _markers.toList()) {
        if (x.position == LatLangPosition) {
          pos = x.markerId.value.toString();
        }
      }
      if (payOrNot != 0 && montant == 0) {}

      if (payOrNot != 0 && montant != 0) {
        modifierMontant(montant, pos);
      }
    }

    setState(() {
      _markers.add(
          Marker(markerId: MarkerId('myPosition'), position: LatLangPosition));
    });
  }

  void _onMapCreated(GoogleMapController controller) {
    setState(() {
      _markers.add(Marker(
          markerId: MarkerId('positionOne'),
          position: LatLng(35.541623, 11.017245)));
      _markers.add(Marker(
          markerId: MarkerId('positionTwo'),
          position: LatLng(35.600962, 11.005035)));

      _markers.add(Marker(
          markerId: MarkerId('positionThree'),
          position: LatLng(35.516964, 11.032476)));
      locatePosition(controller);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("welcome google maps", style: TextStyle(fontSize: 12)),
          actions: [
            TextButton(
              child: Text("logout", style: TextStyle(color: Colors.black)),
              onPressed: () {
                goAway();
              },
            ),
            IconButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Profile(uid: uid)));
                },
                icon: Icon(Icons.person)),
          ],
        ),
        body: GoogleMap(
          onMapCreated: _onMapCreated,
          markers: _markers,
          initialCameraPosition: CameraPosition(target: myPosition, zoom: 15),
        ));
  }

  void goAway() async {
    await _auth.signOut();
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => MyApp()),
    );
  }

  void modifierMontant(int montant, String pos) async {
    DatabaseServices ds = DatabaseServices(uid: uid);
    int nouveauMontant = montant - 2;
    montant = nouveauMontant;
    ds.MetterAJourMontant(uid, nouveauMontant);
  }
  //  ajouterAmendes();
  //       ajouterHistoriqueNonPaye();

}
