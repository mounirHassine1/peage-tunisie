import 'package:flutter/material.dart';
import 'package:peagetunisie/screens/informationsVehicule.dart';
import 'package:peagetunisie/services/authentificationService.dart';

class Sinscrire extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SinscrireState();
}

class SinscrireState extends State<Sinscrire> {
  AuthentificationService _auth = AuthentificationService();
  String nom = "";
  String prenom = "";
  String email = "";
  String motDePasse = "";
  String hello = "";
  Widget _buildNom() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        hintText: 'Quel est ton nom?',
        labelText: 'Nom *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        nom = val;
      },
      validator: (String? value) {
        return (value != null && value.contains('@'))
            ? 'Il y a une erreur'
            : null;
      },
    );
  }

  Widget _buildPrenom() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.person),
        hintText: 'Quel est ton prenom?',
        labelText: 'prenom *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        prenom = val;
      },
      validator: (String? value) {
        return (value != null && value.contains('@'))
            ? 'Il y a une erreur'
            : null;
      },
    );
  }

  Widget _buildEmail() {
    return TextFormField(
      decoration: const InputDecoration(
        icon: Icon(Icons.mail),
        hintText: 'Quel est ton email?',
        labelText: 'Email *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        email = val;
      },
      validator: (String? value) {
        return (value != null && value.contains('@') != false)
            ? 'Il y a une erreur'
            : null;
      },
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      obscureText: true,
      decoration: const InputDecoration(
        icon: Icon(Icons.mail),
        hintText: 'Quel est ton mot de passe?',
        labelText: 'Mot de passe *',
      ),
      onSaved: (String? value) {
        // This optional block of code can be used to run
        // code when the user saves the form.
      },
      onChanged: (val) {
        motDePasse = val;
      },
      validator: (String? value) {
        return (value != null) ? 'Il y a une erreur' : null;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: Padding(
            padding: EdgeInsets.all(20),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              _buildNom(),
              _buildPrenom(),
              _buildEmail(),
              _buildPassword(),
              SizedBox(
                height: 20,
              ),
              RaisedButton(
                  color: Colors.red[200],
                  onPressed: () {
                    createUser();
                    print(email);
                    print(motDePasse);
                  },
                  child: Text("S'inscrire",
                      style: TextStyle(color: Colors.white))),
              Text(hello, style: TextStyle(color: Colors.red))
            ])),
      ),
    );
  }

  void createUser() async {
    dynamic result = await _auth.createNewUser(nom, prenom, email, motDePasse);
    InfosVehicule(
      userID: result,
    );
    if (result == null) {
      setState(() {
        hello = "ooop, vous ne pouvez pas connecter";
      });
    } else {
      setState(() {
        hello = "Merci pour s'inscrire";
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => InfosVehicule(userID: result)),
        );
      });
    }
  }
}
