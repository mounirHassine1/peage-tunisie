import 'package:flutter/material.dart';
import 'package:peagetunisie/screens/historiquePeage.dart';
import 'package:peagetunisie/screens/thePage.dart';

class Profile extends StatefulWidget {
  String uid;
  Profile({required this.uid});
  @override
  State<StatefulWidget> createState() => ProfileState(uid: uid);
}

class ProfileState extends State<Profile> {
  String uid;
  ProfileState({required this.uid});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Mon profile",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
          elevation: 0,
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            color: Colors.black,
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ThePage(uid: uid, payOrNot: 0)));
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
              color: Colors.white,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 50,
                  ),
                  Center(
                      child: Image(
                          image: AssetImage('assets/profile.png'),
                          width: 150,
                          height: 150)),
                  SizedBox(height: 50),
                  Container(
                      decoration: BoxDecoration(
                          color: Colors.lightBlue[200],
                          borderRadius: BorderRadius.circular(20)),
                      width: 300,
                      height: 300,
                      //color: Colors.red,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 20,
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Historique(
                                              uid: uid,
                                            )));
                              },
                              child: const Center(
                                  child: Text('Historique de péages',
                                      style: TextStyle(color: Colors.grey))),
                            ),
                            SizedBox(height: 10),
                            TextButton(
                              style: TextButton.styleFrom(
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              onPressed: () {},
                              child: const Text('Liste amandes',
                                  style: TextStyle(color: Colors.grey)),
                            ),
                            SizedBox(height: 10),
                            TextButton(
                              style: TextButton.styleFrom(
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              onPressed: () {},
                              child: const Text(
                                'Historique des voyages non payées',
                                style: TextStyle(
                                  color: Colors.grey,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            SizedBox(height: 10),
                            TextButton(
                              style: TextButton.styleFrom(
                                textStyle: const TextStyle(fontSize: 20),
                              ),
                              onPressed: () {},
                              child: const Text('Modifier mes informations',
                                  style: TextStyle(color: Colors.grey)),
                            ),
                          ]))
                ],
              )),
        ));
  }
}
