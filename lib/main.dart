import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:peagetunisie/screens/Sinscrire.dart';
import 'package:peagetunisie/screens/seConnecter.dart';
import 'dart:html';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SeConnecter(),
    );
  }
}
