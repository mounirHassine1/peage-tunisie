import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseServices {
  final String uid;
  DatabaseServices({required this.uid});
  //référencer 3 collections
  final CollectionReference AppUsers =
      FirebaseFirestore.instance.collection('users');

  Future updateUserData(
    String nom,
    String prenom,
    String email,
    String motDePasse,
  ) async {
    return await AppUsers.doc(uid).set({
      'nom': nom,
      'prenom': prenom,
      'email': email,
      'motDePasse': motDePasse,
    });
  }

  Future AddVehiculeInfo(
      String marque, String model, String couleur, String matricule) async {
    return await AppUsers.doc(uid).collection("Vehicules").add({
      'userId': uid,
      'marque': marque,
      'model': model,
      'couleur': couleur,
      'matricule': matricule
    });
  }

  Future MetterAJourMontant(String uid, int nouveauMontant) async {
    return await AppUsers.doc(uid).update({'montant': nouveauMontant});
  }

  Future AjouterAmandes(String date, int valeurAmende) async {
    return await AppUsers.doc(uid)
        .collection("Amendes")
        .add({'date': date, 'valeurAmende': valeurAmende});
  }

  Future getDocs() async {}
}
