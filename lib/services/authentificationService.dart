import 'package:firebase_auth/firebase_auth.dart';
import 'package:peagetunisie/services/baseDeDonnees.dart';

class AuthentificationService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  //registration with email and password
  Future createNewUser(
      String nom, String prenom, String email, String motDePasse) async {
    try {
      UserCredential result = await _auth.createUserWithEmailAndPassword(
          email: email, password: motDePasse);
      User? user = result.user;
      await DatabaseServices(uid: user!.uid)
          .updateUserData(nom, prenom, email, motDePasse);
      return user.uid;
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  //signin with email and password
  //FirebaseUser user = result.user;
  Future signIn(String email, String password) async {
    try {
      UserCredential result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      User? user = result.user;
      return user!.uid;
      //I have to return the user uid !!!!
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  //signout
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }
}
